<?php 
include_once 'session.php';
include 'lib/Database.php'; 

class User{
	private $db;
	public function __construct(){
		$this->db = new Database();
	}


/*------------------------------------------ Email chekcing ------------------------------------------*/

	public function emailChecking($email){
		$sql = "SELECT * FROM tbl_user WHERE email = :email";
		$query = $this->db->pdo->prepare($sql);
		$query->bindValue(':email', $email);
		$query->execute();
		if ($query->rowCount() > 0) {
			return true;
		}else{
			return false;
		}
	}


/*------------------------------------------ User Registration ------------------------------------------*/


	public function userRegistration($data){
		$name = $data['name'];
		$username = $data['username'];
		$email = $data['email'];
		$password = md5($data['password']);


		$chk_email = $this->emailChecking($email);

		if ($name == "" OR $username == "" OR $email == "" OR $password == "") {
			$msg = "<div class='alert alert-danger'><strong>Error ! </strong>Field must not empty!!</div>";
			return $msg;
		}

		if ($chk_email == true) {
			$msg = "<div class= 'alert alert-danger'><strong>Error!<strong>Email already exits!!</div>";
			return $msg;
		}


		$sql = "INSERT INTO tbl_user(name, username, email, password) VALUES(:name, :username, :email, :password)";
		$query = $this->db->pdo->prepare($sql);
		$query->bindValue(':name', $name);
		$query->bindValue(':username', $username);
		$query->bindValue(':email', $email);
		$query->bindValue(':password', $password);
		$result = $query->execute();
		if ($result) {
			$msg = "<div class='alert alert-success'><strong>Success!<strong> Thank you, You have been registerted</div>";
			return $msg;
			header("Location: register.php");
		}else{
			$msg = "<div class='alert alert-Error'><strong>Error!<strong>Sorry, There have been something error to register details</div>";
			return $msg;
		}
	}




	/*------------------------------------------ Email and password for login ------------------------------------------*/



	public function getLoginUser($email, $password){
		$sql = "SELECT * FROM tbl_user WHERE email = :email AND password = :password";
		$query = $this->db->pdo->prepare($sql);
		$query->bindValue(':email', $email);
		$query->bindValue(':password', $password);
		$query->execute();
		$result =$query->fetch(PDO::FETCH_OBJ);
		return $result;
	}




	public function passChecking($password){
		$sql = "SELECT * FROM tbl_user WHERE password = :password";
		$query = $this->db->pdo->prepare($sql);
		$query->bindValue(':password', $password);
		$query->execute();
		$result = $query->fetch(PDO::FETCH_OBJ);
		return $result;
	}


	public function userLogin($data){

		$email = $data['email'];
		$password = md5($data['password']);

		$chk_email = $this->emailChecking($email);
		$user_pass = $this->passChecking($password);


		if ($email == "" OR $password == "") {
			$msg = "<div class='alert alert-danger'><strong>Error ! </strong>Field must not empty!!</div>";
			return $msg;
		}

		if ($chk_email == false) {
			$msg = "<div class= 'alert alert-danger'><strong>Error!<strong>Email not exits!!</div>";
			return $msg;
		}

		if ($user_pass == false) {
			$msg = "<div class= 'alert alert-danger'><strong>Error!<strong>Password not exits!!</div>";
			return $msg;
		}

		$result = $this->getLoginUser($email, $password);

		if ($result) {
			Session::init();
			Session::set("login", true);
			Session::set("id", $result->id);
			Session::set("name", $result->name);
			Session::set("username",$result->username);
			Session::set("email", $email);
			Session::set("loginmsg",  "<div class='alert alert-success'><strong>Success! </strong>Thank You, You are logged in!!</div>");
			header("Location: index.php");
		}else{
			$msg = "<div class='alert alert-danger'><strong>Error! </strong>previous password changed!!</div>";
			return $msg;
		}


	}



	/*------------------------------------------ Email and password for login ------------------------------------------*/


	public function getUserinfo(){
		$sql = "SELECT * FROM tbl_user ORDER BY id ASC";
		$query = $this->db->pdo->prepare($sql);
		$query->execute();
		$result = $query->fetchAll();
		return $result;
	}



/*------------------------------------------ Email and password for login ------------------------------------------*/



public function userDataByid($id){
		$sql = "SELECT * FROM tbl_user WHERE id = :id LIMIT 1";
		$query = $this->db->pdo->prepare($sql);
		$query->bindValue(':id', $id);
		$query->execute();
		$result = $query->fetch(PDO::FETCH_OBJ);
		return $result;
	}



/*------------------------------------------ Email and password for login ------------------------------------------*/



	public function userdataUpdate($id, $data){
		$name = $data['name'];
		$username = $data['username'];
		$email = $data['email'];


		if ($name == "" OR $username == "" OR $email == "") {
			$msg = "<div class='alert alert-danger'><strong>Error!<strong>Sorry, Field must not empty!</div>";
			return $msg;
		}

		$userdataChange = $this->userDataByid($id);

		if ($name == $userdataChange->name AND $username == $userdataChange->username AND $email == $userdataChange->email) {
			$msg = "<div class='alert alert-danger'><strong>Error! </strong>Without changing profile not update!!</div>";
			return $msg;
		}



		$sql = "UPDATE tbl_user SET 
		name = :name,
		username = :username,
		email = :email
		WHERE id = :id";
		$query = $this->db->pdo->prepare($sql);
		$query->bindValue(':id', $id);
		$query->bindValue(':name', $name);
		$query->bindValue(':username', $username);
		$query->bindValue(':email', $email);
		$result = $query->execute();
		if ($result) {	
		$msg = "<div class='alert alert-success'><strong>Success!<strong> Thank you, Userdata Successfully updated</div>";
			return $msg;
		}else{
			$msg = "<div class='alert alert-danger'><strong>Error!<strong>Sorry, There have been something error to update details</div>";
			return $msg;
		}
	}




/*------------------------------------------ Email and password for login ------------------------------------------*/



	public function changePass($id, $data){

		$old_pass = $data['old_password'];
		$new_pass = $data['password'];
		$confirm_pass = $data['con_password'];


		if ($old_pass == "" OR $new_pass == "" OR $confirm_pass == "") {
			$msg = "<div class='alert alert-danger'><strong>Error!<strong>Sorry, Field must not empty!</div>";
			return $msg;
		}





		$old_pass = md5($data['old_password']);
		$new_pass = md5($data['password']);
		$confirm_pass = md5($data['con_password']);

		$userOldpass = $this->userDataByid($id);
		if ($old_pass != $userOldpass->password) {
			$msg = "<div class='alert alert-danger'><strong>Error!<strong> Sorry, old_password does not matched!</div>";
			return $msg;
		}

		if ($new_pass != $confirm_pass) {
			$msg = "<div class='alert alert-danger'><strong>Error!<strong> Sorry, new password does not matched!</div>";
			return $msg;
		}

		$sql = "UPDATE tbl_user SET
		password = :password
		WHERE id = :id
		";
		$query = $this->db->pdo->prepare($sql);
		$query->bindValue(':id', $id);
		$query->bindValue(':password', $new_pass);
		$result = $query->execute();
		if ($result) {	
		$msg = "<div class='alert alert-success'><strong>Success!<strong> Thank you, Password changed Successfully </div>";
			return $msg;
		}else{
			$msg = "<div class='alert alert-danger'><strong>Error!<strong>Sorry, There have been something error to update details</div>";
			return $msg;
		}
	}




		public function searchUserData($data){
		$sql = "SELECT * FROM tbl_user WHERE id LIKE '%$data%' OR name LIKE '%$data%' OR email LIKE '%$data%'";
		$query = $this->db->pdo->prepare($sql);
		$query->execute();
		$result = $query->fetchAll();
		return $result;
	}



	public function userDataDelete($id){
		$sql = "DELETE FROM tbl_user WHERE id = :id ";
		$query = $this->db->pdo->prepare($sql);
		$query->bindValue(':id',$id);
		$result = $query->execute();
		return $result;
	}

}
?>