<?php 

// 
include "inc/header.php";
include "lib/user.php";

// Seesioin
Session::checkSession(); 

// Login Message
$loginmsg = Session::get("loginmsg");

if (isset($loginmsg)) {
  echo $loginmsg;
}
Session::set("loginmsg", NULL); 
$user = new User();
 
// Display all data
if (isset($_POST['search'])) {
  $tabledata = $user->searchUserData($_POST['search']);
}else{
  $tabledata = $user->getUserinfo();
}



// Delete user data

if (isset($_POST['delete'])) {
  $deleteUser = $user->userDataDelete($_POST['data_id']);


}
  
?> 





<section class="registerSection">
  <div class="container">
    <div class="card">
      <div class="card-header">
        <div class="row">
          <div class="col-md-6">
            <div class="allheading">
               <h3>Userlist</h3>
            </div>
          </div>
          <div class="col-md-4">
            <div class="allheading">
              <form action="" method="POST">
                 <input type="text" name="search" required/>
                 <input type="submit" value="Search" name="btnsearch" />
              </form>
            </div>
          </div>
          <div class="col-md-2">
            <div class="allheading">
               <h3 class="welcomeTab"><strong>Welcome!</strong> <?php $name = Session::get('name'); if (isset($name)) {
          echo $name;
        } ?></h3>
            </div>
          </div>
        </div>
      </div>
      <div class="card-body">
        <table class="table table-striped">
          <tr>
            <th width="10%">Serial</th>
            <th width="20%">Name</th>
            <th width="20%">Username</th>
            <th width="20%">Email Address</th>
            <th width="20%">Action</th>
            <th width="10">Delete</th>
          </tr>

          <?php 
            foreach ($tabledata as $data) { ?>

          <tr>
            <td><?php echo $data['id']; ?></td>
            <td><?php echo $data['name']; ?></td>
            <td><?php echo $data['username']; ?></td>
            <td><?php echo $data['email']; ?></td>
            <td><a class="btn btn-primary" href="profile.php?id=<?php echo $data['id']; ?>">Edit</a></td>
            <td>
              <form action="" method="POST">
                    <?php if ($data['id']) { ?>
                      <input type="hidden" name="data_id" value="<?= $data['id'] ?>">
                    <button class="btn btn-danger" type="submit" name="delete">Delete</button>
                    <?php  } ?>
                   
                   
              </form>
            </td>
          </tr>
              
          <?php  } ?>

        </table>
      </div>
    </div>
  </div>
</section>

<?php include "inc/footer.php"; ?>

