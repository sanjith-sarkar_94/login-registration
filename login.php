
<link rel="stylesheet" type="text/css" href="css/login.css">
<?php include "inc/header.php"; ?>
<?php include 'lib/user.php'; ?>
<?php Session::checkLogin(); ?>

<?php 
$user = new User;
if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['login'])) {
  $userLogin = $user->userLogin($_POST);
}
 ?>

<section class="loginSection">
  <div class="container">
    <div class="card">
      <div class="card-header">
        <h3 class="text-center">User Login</h3>
      </div>

      <div class="card-body">
        <?php 
        if (isset($userLogin)) {
           echo $userLogin;
         } ?>
        <form action="" method="POST">
          <div class="form-group">
            <label for="email">Email Address</label>
            <input type="email" name="email" id="email" class="form-control">
          </div>

          <div class="form-group">
            <label for="password">Password</label>
            <input type="password" name="password" id="password" class="form-control">
          </div>
          <div class="form-group text-center">
            <button type="submit" class="btn btn-success mt-3" id="login" name="login">Login</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</section>


<?php include "inc/footer.php"; ?>

