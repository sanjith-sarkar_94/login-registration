
<?php include "inc/header.php"; ?>
<?php include "lib/user.php"; ?>
<link rel="stylesheet" type="text/css" href="css/profile.css">


<?php 
if (isset($_GET['id'])) {
  $userid = (int)$_GET['id'];
   $sesid = Session::get('id');
    if($userid != $sesid){
      header("Location: index.php");
    }
}
?>


<?php 
$user = new User();
if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['update'])) {
  $chg_pass = $user->changePass($userid, $_POST);
  
}
?>



<section class="passwordSection">
  <div class="container">
    <div class="card">
      <div class="card-header">
        <h2 class="text-center">Change Password</h2>
      </div>

        <div class="card-body">
              <?php if (isset($chg_pass)) {
        echo $chg_pass;
      } ?>

          <form action="" method="POST">
            <div class="form-group">
              <label>Old Password</label>
              <input type="password" name="old_password" id="old_password" class="form-control">
            </div>

            <div class="form-group">
              <label>New Password</label>
              <input type="password" name="password" id="password" class="form-control">
            </div>

            <div class="form-group">
              <label>Confirm Password</label>
              <input type="password" name="con_password" id="con_password" class="form-control">
            </div>

            <div class="form-group text-center mt-2">
              <button type="submit" name="update" class="btn btn-success">Update</button>
            </div>

          </form>
        </div>
    </div>
  </div>
</section>


<?php include "inc/footer.php"; ?>

