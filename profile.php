
<?php include "inc/header.php"; ?>
<?php include "lib/user.php"; ?>
<link rel="stylesheet" type="text/css" href="css/profile.css">


<?php 

if (isset($_GET['id'])) {
  $userid = (int)$_GET['id'];
}
$user = new User();
 ?>

 <?php 
if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['update'])) {
    $userdataUpdate = $user->userdataUpdate($userid, $_POST);
}

  ?>

<section class="loginSection">
  <div class="container">
    <div class="card">
      <div class="card-header">
        <h3>User Profile <span class="welcomeTab"><a class="btn btn-success" href="index.php">Back</a></span></h3>
      </div>

      <?php 

      $userdata = $user->userDataByid($userid);
      if ($userdata) {

       ?>
        
        <div class="card-body">
         <form action="" method="POST">

          <?php if (isset($userdataUpdate)) {
            echo $userdataUpdate;
          } ?>

          <div class="form-group">
           <label for="name">Name</label>
           <input type="text" name="name" id="name" class="form-control" required="" value="<?php echo $userdata->name; ?>">
         </div>

         <div class="form-group">
           <label for="username">Username</label>
           <input type="text" name="username" id="username" class="form-control" required="" value="<?php echo $userdata->username; ?>">
         </div>

         <div class="form-group">
           <label for="email">Email Address</label>
           <input type="text" name="email" id="email" class="form-control" required="" value="<?php echo $userdata->email; ?>">
         </div>

         <?php 

         $id = Session::get('id');
         if ($userid == $id) {
          ?>

          <button type="submit" name="update" class="btn  btn-success">Update</button>
          <a class="btn btn-info" href="changepass.php?id=<?php echo $userid; ?>">change password</a>
        <?php } ?>
      </form>
  </div>
<?php } ?>
</div>
</div>
</section>




<?php include "inc/footer.php"; ?>

