
<link rel="stylesheet" type="text/css" href="css/register.css">
<?php include "inc/header.php"; ?>
<?php include "lib/user.php"; ?>

<?php 
$user = new User;
if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['register'])) {
  $userRegister = $user->userRegistration($_POST);
}
?>

<section class="registerSection">
  <div class="container">
    <div class="card">
      <div class="card-header">
        <h3 class="text-center"><strong>User Registration</strong></h3>
      </div>
      <div class="card-body">
        <?php 
        if (isset($userRegister)) {
          echo $userRegister;
        }
        ?>
        <form action="" method="POST">
          <div class="form-group">
            <label for="name" >Name</label>
            <input type="text" name="name" id="name" class="form-control">
          </div>

          <div class="form-group">
            <label for="username">Username</label>
            <input type="text" name="username" id="username" class="form-control">
          </div>

          <div class="form-group">
            <label for="email">Email Address</label>
            <input type="text" name="email" id="email" class="form-control">
          </div>

          <div class="form-group">
            <label for="password">Password</label>
            <input type="password" name="password" id="password" class="form-control">
          </div>

          <div class="form-group text-center">
            <button type="submit" class="btn btn-success mt-3" id="register" name="register">Register</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</section>
<?php include "inc/footer.php"; ?>

