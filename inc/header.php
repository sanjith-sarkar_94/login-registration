<?php 
$filepath = realpath(dirname(dirname(__FILE__)));
include_once $filepath.'/lib/Session.php';
Session::init();
?>   

<!doctype html>
  <html class="no-js" lang="">

  <head>
    <meta charset="utf-8">
    <title></title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta property="og:title" content="">
    <meta property="og:type" content="">
    <meta property="og:url" content="">
    <meta property="og:image" content="">

    <link rel="manifest" href="site.webmanifest">
    <link rel="apple-touch-icon" href="icon.png">
    <!-- Place favicon.ico in the root directory -->

    <link rel="stylesheet" href="css/normalize.css">
    <link rel="stylesheet" href="css/main.css">
    <link rel="stylesheet" type="text/css" href="css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="css/all.min.css">
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.min.js"></script>
    <!-- <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet"/>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script> -->

    <meta name="theme-color" content="#fafafa">
  </head>

  <body>


    <!--------------------------------- Header Section ---------------------------------->

   <?php 
   if (isset($_GET['action']) && $_GET['action'] == 'logout') {
      Session::destroy();
   }
    ?>



    <header class="headerSection">  
      <div class="container">
       <nav class="navbar navbar-default navbar-expand-lg navbar-light bg-light">
        <div class="container-fluid">
          <div class="navbar-header">
            <a class="navbar-brand" href="index.php">Login Register System PHP & PDO</a>
          </div>
          <ul class="nav navbar-nav float-right">
            <?php 
            $id = Session::get('id');
            $userLogin = Session::get('login');
            if ($userLogin == true) { ?>
                <li class="nav-item"><a class="nav-link active" href="profile.php?id=<?php echo $id; ?>">Profile</a></li>
            <li class="nav-item"><a class="nav-link active" href="?action=logout">Logout</a></li>
             <?php }else{ ?>

              <li class="nav-item"><a class="nav-link active" href="login.php">Login</a></li>
            <li class="nav-item"><a class="nav-link active" href="register.php">Register</a></li>
            <?php } ?>
            
           
          </ul>
        </div>
      </nav>
    </div>
  </header> 